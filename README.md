# Learning with a web scraping project

## Objectif : pourquoi un projet de Web-Scraping ?

### Apprendre / étudier / approfondir

- Ce projet a un but d'apprentissage :
  - Il s'agit principalement de rassembler certaines technologies & pratiques de travail afin de comprendre leur utilité
  - Aucune finalité particulière dans le scraping de données
  - Apprendre / étudier / approfondir les "bonnes pratiques" du scraping

### Langages, Outils, Technos

- Développement du code :

  - une partie en PYTHON
  - une partie en JAVASCRIPT (ou TypeScript) avec NODE.JS

- Outils, Technos :
  - Selenium
  - Puppeteer
  - ElasticSearch
  - RabbitMQ

## Détail du project

- Développement en PYTHON :

  - verifier version :
    `python --version`
  - sinon installer :

    `sudo apt-get install python3.7`
    ou installer le paquet de developpement :
    `sudo apt-get install python-dev`

* script Python (Shebang en 1ere ligne)
  `file.py`

```py
#!/bin/usr/python

print("Hey Oh!")
```
